package model

type (
	Model struct {
		db
	}
)

func New(db db) *Model {
	return &Model{
		db: db,
	}
}

func (m *Model) Carrier() ([]*Carrier, error) {
	return m.SelectCarriers()
}

func (m *Model) CargoType() ([]*CargoType, error) {
	return m.SelectCargoTypes()
}
