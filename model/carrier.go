package model

import (
	"github.com/satori/go.uuid"
)

type Carrier struct {
	Id       uuid.UUID `json:"id,omitempty" db:"ID"`
	Name     string    `json:"name,omitempty" db:"NAME"`
	FullName string    `json:"full_name,omitempty" db:"FULLNAME"`
	INN      string    `json:"inn,omitempty" db:"INN"`
	KPP      string    `json:"kpp,omitempty" db:"KPP"`
}
