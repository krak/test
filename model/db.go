package model

type db interface {
	SelectCarriers() ([]*Carrier, error)
	SelectCargoTypes() ([]*CargoType, error)
}
