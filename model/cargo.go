package model

import (
	"gopkg.in/guregu/null.v3"

	"github.com/gofrs/uuid"
)

type CargoType struct {
	Id          uuid.UUID     `json:"id,omitempty" db:"ID"`
	ParentId    uuid.NullUUID `json:"parent_id,omitempty" db:"PARENTID"`
	Carrier     uuid.NullUUID `json:"carrier,omitempty" db:"CARRIER"`
	Code        null.String   `json:"code,omitempty" db:"CODE"`
	Name        null.String   `json:"name,omitempty" db:"NAME"`
	Information null.String   `json:"information,omitempty" db:"INFORMATION"`
}
