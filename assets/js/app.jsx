
// -*- JavaScript -*-


class CarrierItem extends React.Component {
  render() {
    return (
      <tr>
        <td> {this.props.id}    </td>
        <td> {this.props.name} </td>
        <td> {this.props.fullname}  </td>
        <td> {this.props.inn}  </td>
        <td> {this.props.kpp}  </td>
      </tr>
    );
  }
}

class CarrierList extends React.Component {
  constructor(props) {
    super(props);
    this.state = { carrier: [] };
  }

  componentDidMount() {
    this.serverRequest =
      axios
        .get("/carrier")
        .then((result) => {
           this.setState({ carrier: result.data });
        });
  }

  render() {
    const carrier = this.state.carrier.map((carrier, i) => {
      return (
        <CarrierItem key={i} id={carrier.Id} name={carrier.Name} fullname={carrier.FullName} inn={carrier.INN} kpp={carrier.KPP} />
      );
    });

    return (
      <div>
        <table><tbody>
          <tr><th>Id</th><th>Name</th><th>Full name</th><th>ИНН</th><th>КПП</th></tr>
          {carrier}
        </tbody></table>

      </div>
    );
  }
}

ReactDOM.render( <CarrierList/>, document.querySelector("#root"));
