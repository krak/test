package db

import (
	"log"

	"github.com/jmoiron/sqlx"

	"gitlab.com/krak/test/model"

	_ "github.com/nakagami/firebirdsql"
)

type (
	Config struct {
		ConnectString string
	}

	fbDb struct {
		dbConn *sqlx.DB

		sqlSelectCarrier    *sqlx.Stmt
		sqlSelectCargoTypes *sqlx.Stmt
	}
)

func InitDb(cfg Config) (*fbDb, error) {
	if dbConn, err := sqlx.Connect("firebirdsql", cfg.ConnectString); err != nil {
		return nil, err
	} else {
		f := &fbDb{dbConn: dbConn}
		if err := f.dbConn.Ping(); err != nil {
			return nil, err
		}
		if err := f.prepareSqlStatements(); err != nil {
			return nil, err
		}
		return f, nil
	}
}

func (f *fbDb) prepareSqlStatements() (err error) {

	if f.sqlSelectCarrier, err = f.dbConn.Preparex(
		"SELECT Id, Name, full_name as FullName, INN, KPP FROM carriers",
	); err != nil {
		return err
	}
	if f.sqlSelectCargoTypes, err = f.dbConn.Preparex(
		`SELECT
			id,
			parent_id as parentid,
			carrier,
			code,
			Name,
			information
		FROM cargo_types
		`,
	); err != nil {
		return err
	}

	return nil
}

func (f *fbDb) SelectCarriers() ([]*model.Carrier, error) {
	carrier := make([]*model.Carrier, 0)
	if err := f.sqlSelectCarrier.Select(&carrier); err != nil {
		log.Printf("Error in sql: %v", err)
		return nil, err
	}
	return carrier, nil
}

func (f *fbDb) SelectCargoTypes() ([]*model.CargoType, error) {
	cargo_type := make([]*model.CargoType, 0)
	if err := f.sqlSelectCargoTypes.Select(&cargo_type); err != nil {
		log.Printf("Error in sql: %v", err)
		return nil, err
	}
	return cargo_type, nil
}
